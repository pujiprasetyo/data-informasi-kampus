# Data Informasi Kampus

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.24.

Aplikasi ini sebagai frontend yang akan menerima json dari web server REST API. Aplikasi ini dibuat dengan angular 8.3.24 dan angular material sebagai framework styling. Framework angular ini merupakan produk dari google. Konsep framework ini menggunakan component base.

  - Angular 8.3.24
  - Angular Material

# Features!

  - Melihat seluruh data dalam bentuk tabel yang dilengkapi dengan pagination dan sortir
  - Melakukan input data baru
  - Melakukan update pada sebuah data
  - Melakukan  delete pada sebuah data per-npm (one by one)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

