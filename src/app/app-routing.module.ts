import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DosenComponent } from './dosen/dosen.component';
import { MahasiswaComponent } from './mahasiswa/mahasiswa.component';

// untuk malukukan route navigasi
const routes: Routes = [
  { path: 'dosen', component: DosenComponent },
  { path: 'mahasiswa', component: MahasiswaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
