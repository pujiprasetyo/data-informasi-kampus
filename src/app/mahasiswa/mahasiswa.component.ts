import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MahasiswaService } from '../service/mahasiswa.service';
import { Mahasiswa } from '../model/mahasiswa';
import { MatDialog } from '@angular/material';
import { MahasiswaPostComponent } from '../mahasiswa-post/mahasiswa-post.component';

@Component({
  selector: 'app-mahasiswa',
  templateUrl: './mahasiswa.component.html',
  styleUrls: ['./mahasiswa.component.scss']
})
export class MahasiswaComponent implements OnInit {

  constructor(
    private mahasiswaService: MahasiswaService,
    public dialog: MatDialog) { }

  displayedColumns = ['npm', 'nama', 'kelas', 'action']
  dataSource: any;
  mahasiswa: Mahasiswa;

  tablesortParam: any;
  tablesortTitle: any;
  tablepageIndex: any;
  tablepageSize: any;
  numberOfRows: number;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit() {
    this.getPagination({
      pageIndex: 0,
      pageSize: 2,
      length: 0
    });
  }

  getPagination(event: PageEvent) {
    this.tablesortTitle = 'npm';
    this.tablesortParam = 'DESC';
    this.tablepageIndex = event.pageIndex + 1;
    this.tablepageSize = event.pageSize;
    this.getTable();
  }

  getTable() {
    this.mahasiswaService.getTableData(
      this.tablesortTitle,
      this.tablesortParam,
      this.tablepageIndex,
      this.tablepageSize
    ).subscribe(
      res => {
        this.dataSource = res;
        this.numberOfRows = this.dataSource.numberOfRows;
        this.dataSource = this.dataSource.dataTable;
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MahasiswaPostComponent, {
      width: '45%',
      data: null
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.ngOnInit();
        console.log(res);
      });
  }

  getMahasiswaDetail(npm: string) {
    this.mahasiswaService.getSatuMahasiswa(npm)
      .subscribe(res => {
        this.mahasiswa = res;
        const dialogRef = this.dialog.open(MahasiswaPostComponent, {
          width: '45%',
          data: this.mahasiswa
        });
        dialogRef.afterClosed()
          .subscribe(res => {
            this.ngOnInit();
            console.log(res);
          });
      });
  }

  deleteMahasiswaDetail(npm: string) {
    this.mahasiswaService.deleteDataMahasiswa(npm)
      .subscribe(res => {
        this.ngOnInit();
        console.log('Data has been deleted', res);
      });
  }
}
