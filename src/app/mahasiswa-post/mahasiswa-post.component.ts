import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MahasiswaService } from '../service/mahasiswa.service';
import { FormBuilder, Validators, FormGroup, NgForm, FormControl } from '@angular/forms';
import { Mahasiswa } from '../model/mahasiswa';

@Component({
  selector: 'app-mahasiswa-post',
  templateUrl: './mahasiswa-post.component.html',
  styleUrls: ['./mahasiswa-post.component.scss']
})
export class MahasiswaPostComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<MahasiswaPostComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Mahasiswa,
    private api: MahasiswaService,
    private formBuilder: FormBuilder) { }

  mahasiswaForm = new FormGroup({
    npm: new FormControl(),
    nama: new FormControl(),
    kelas: new FormControl()
  });

  ngOnInit() {
    this.mahasiswaForm = this.formBuilder.group({
      'npm': [null, Validators.required],
      'nama': [null, Validators.required],
      'kelas': [null, Validators.required]
    });
    if (this.data != null) {
      this.getDataMahasiswa();
    }
  }

  onFormSubmit(form: NgForm) {
    this.api.addDataMahasiswa(form)
      .subscribe(res => {
        console.log('onFormSubmit : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmit : ', err)
      });
  }

  onFormSubmitUpdate(form: NgForm) {
    this.api.updateDataMahasiswa(this.data.npm, form)
      .subscribe(res => {
        console.log('onFormSubmitUpdate : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmitUpdate : ', err)
      });
  }

  getDataMahasiswa() {
    this.mahasiswaForm.setValue({
      npm: this.data.npm,
      nama: this.data.nama,
      kelas: this.data.kelas
    });
  }
}
