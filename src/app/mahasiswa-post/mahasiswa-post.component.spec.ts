import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MahasiswaPostComponent } from './mahasiswa-post.component';

describe('MahasiswaPostComponent', () => {
  let component: MahasiswaPostComponent;
  let fixture: ComponentFixture<MahasiswaPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MahasiswaPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MahasiswaPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
