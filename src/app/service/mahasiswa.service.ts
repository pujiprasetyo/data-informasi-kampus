import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mahasiswa } from '../model/mahasiswa';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MahasiswaService {

  constructor(private http: HttpClient) {
  }

  private apiUrl = "http://localhost:8090/api/mahasiswa";

  getDataMahasiswa(): Observable<Mahasiswa[]> {
    return this.http.get<Mahasiswa[]>(this.apiUrl);
  }

  getSatuMahasiswa(npm: string): Observable<Mahasiswa> {
    const url = `${this.apiUrl}/${npm}`;
    return this.http.get<Mahasiswa>(url)
  }

  addDataMahasiswa(mahasiswa: any): Observable<Mahasiswa> {
    return this.http.post<Mahasiswa>(this.apiUrl, mahasiswa, httpOptions);
  }

  updateDataMahasiswa(npm: string, mahasiswa: any): Observable<any> {
    const url = `${this.apiUrl}/${npm}`;
    return this.http.put(url, mahasiswa, httpOptions);
  }

  deleteDataMahasiswa(npm: string): Observable<Mahasiswa> {
    const url = `${this.apiUrl}/${npm}`;
    return this.http.delete<Mahasiswa>(url, httpOptions);
  }

  // mulai dari sini editnya
  // getTableData(
  //   sortTitle?: any,
  //   sortParam?: any,
  //   pageIndex?: any,
  //   pageSize?: any
  // ): Observable<Mahasiswa[]> {
  //   return this.http.get<Mahasiswa[]>(
  //     `${this.apiUrl}?col=${sortTitle}&sort=${sortParam}&page=${pageIndex}&limit=${pageSize}`, httpOptions
  //   );
  // }

  getTableData(
    sortTitle?: any,
    sortParam?: any,
    pageIndex?: any,
    pageSize?: any
  ) {
    return this.http.get(
      `${this.apiUrl}?col=${sortTitle}&sort=${sortParam}&page=${pageIndex}&limit=${pageSize}`, httpOptions
    );
  }

}
